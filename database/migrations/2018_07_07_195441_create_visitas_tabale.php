<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitasTabale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('paciente_id');
            $table->string('visita_detalle', 255);
            //$table->dateTime('visita_fecha');
            $table->timestamps();

            $table->foreign('paciente_id')
                    ->references('id')->on('pacientes')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                    
            $table->index(['paciente_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
    }
}
