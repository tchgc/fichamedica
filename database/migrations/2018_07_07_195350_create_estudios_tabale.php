<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiosTabale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('paciente_id');
            $table->string('estudio_detalle', 255);
            //$table->dateTime('estudio_fecha');
            $table->timestamps();

            $table->foreign('paciente_id')
                    ->references('id')->on('pacientes')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                    
            $table->index(['paciente_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios');
    }
}
