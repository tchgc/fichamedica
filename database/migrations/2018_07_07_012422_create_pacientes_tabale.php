<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTabale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paciente_nombre', 255);
            $table->string('paciente_apellido', 255);
            $table->string('paciente_sexo', 255);
            $table->date('paciente_nacimiento');
            $table->string('paciente_dni', 255)->nullable();
            $table->string('paciente_telefono',255)->nullable();
            $table->string('paciente_extra',255)->nullable();
            $table->timestamps();
            $table->index(['id']);
            $table->index(['paciente_nombre']);
            $table->index(['paciente_apellido']);
            $table->index([ 'paciente_dni']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
