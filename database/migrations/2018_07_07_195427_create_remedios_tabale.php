<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemediosTabale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remedios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('paciente_id');
            $table->string('remedio_nombre', 255);
            $table->string('remedio_droga', 255)->nullable();
            //$table->dateTime('remedio_fecha');
            $table->timestamps();

            $table->foreign('paciente_id')
                    ->references('id')->on('pacientes')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->index(['paciente_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remedios');
    }
}
