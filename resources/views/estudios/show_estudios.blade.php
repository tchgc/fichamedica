@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>{{$paciente->paciente_nombre}} {{$paciente->paciente_apellido}}</h1>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            Estudios
                        </div>
                        <div class="col-md-2">
                            <a class="card-link" href="{{ route('ver_paciente', [ 'paciente_id' => $paciente_id ]) }}">VOLVER</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>Fecha</th>
                            <th>Detalle</th>
                        </tr>
                        @foreach ($estudios as $estudio)
                        <tr>
                            <td>{{  date('d-m-Y', strtotime($estudio->created_at)) }}</td>
                            <td>{{ $estudio->estudio_detalle }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            <div class="clearfix"></div>
            {{$estudios->links()}}
        </div>
    </div>
</div>
@endsection