@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8 col-md-7">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">Cargar Paciente</div>
                    <div class="card-body">
                        <!--<form method="POST" action="{{ route('guardar_paciente') }}" class="text-left form-email" data-success="Paciente Cargado" data-error="Faltan llenar campos." data-success-redirect="{{url('/pacientes')}}">-->
                            <form method="POST" action="{{ route('guardar_paciente') }}" aria-label="Cargar Paciente">
                            @csrf
                            <div class="form-group row">
                                <label for="paciente_nombre" class="col-md-4 col-form-label text-md-right">Nombre*:</label>
    
                                <div class="col-md-6">
                                    <input id="paciente_nombre" type="text" class="form-control{{ $errors->has('paciente_nombre') ? ' is-invalid' : '' }}" name="paciente_nombre" value="{{ old('paciente_nombre') }}" required autofocus>
    
                                    @if ($errors->has('paciente_nombre'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('paciente_nombre') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                                <!--
                            <div class="col-sm-6"> 
                                <label>Nombre:</label> 
                                <input name="paciente_nombre" class="validate-required" type="text"> 
                            </div>
                        -->
                            <!--<div class="col-sm-6"> <label>Apellido:</label> <input name="paciente_apellido" class="validate-required" type="text"> </div>-->
                            <div class="form-group row">
                                <label for="paciente_apellido" class="col-md-4 col-form-label text-md-right">Apellido*:</label>
                                <div class="col-md-6">
                                    <input id="paciente_apellido" type="text" class="form-control{{ $errors->has('paciente_apellido') ? ' is-invalid' : '' }}" name="paciente_apellido" value="{{ old('paciente_apellido') }}" required autofocus>
                                    @if ($errors->has('paciente_apellido'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('paciente_apellido') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">                                        
                                <label for="paciente_sexo" class="col-md-4 col-form-label text-md-right">Sexo*:</label>
                                <div class="col-md-6">
                                    <select id="paciente_sexo" name="paciente_sexo" class="selectpicker {{ $errors->has('paciente_sexo') ? ' is-invalid' : '' }} " value="{{ old('paciente_sexo') }}" required>
                                        <option data-tokens=""></option>
                                        <option data-tokens="hombre">Hombre</option>
                                        <option data-tokens="mujer">Mujer</option>
                                    </select> 
                                    @if ($errors->has('paciente_sexo'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('paciente_sexo') }}</strong>
                                        </span>
                                    @endif
                                </div>                                         
                            </div>
                            <!--<div class="col-sm-6"> <label>DNI:</label> <input name="paciente_dni"  class="validate-required validate-number-dash" type="text"> </div>-->
                            <div class="form-group row">
                                <label for="paciente_dni" class="col-md-4 col-form-label text-md-right">DNI:</label>
                                <div class="col-md-6">
                                    <input id="paciente_dni" type="text" class="form-control{{ $errors->has('paciente_dni') ? ' is-invalid' : '' }}" name="paciente_dni" value="{{ old('paciente_dni') }}" autofocus>
                                    @if ($errors->has('paciente_dni'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('paciente_dni') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!--<div class="col-sm-6"> <label>Teléfono:</label> <input name="paciente_telefono" type="text"> </div>-->
                            <div class="form-group row">
                                <label for="paciente_telefono" class="col-md-4 col-form-label text-md-right">Teléfono:</label>
                                <div class="col-md-6">
                                    <input id="paciente_telefono" type="text" class="form-control{{ $errors->has('paciente_telefono') ? ' is-invalid' : '' }}" name="paciente_telefono" value="{{ old('paciente_telefono') }}" autofocus>
                                    @if ($errors->has('paciente_telefono'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('paciente_telefono') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!--<div class="col-sm-12"> <label></label> <textarea rows="6" name="paciente_extra"></textarea> </div>-->
                            <div class="form-group row">
                                <label for="paciente_extra" class="col-md-4 col-form-label text-md-right">Extra:</label>
                                <div class="col-md-6">
                                    <textarea id="paciente_extra" class="form-control{{ $errors->has('paciente_extra') ? ' is-invalid' : '' }}" name="paciente_extra" autofocus>{{ old('paciente_extra') }}</textarea>
                                    @if ($errors->has('paciente_extra'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('paciente_extra') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="paciente_nacimiento" class="col-md-4 col-form-label text-md-right">Fecha Nacimiento:</label>
                                <div class="col-md-6">
                                    <input id="paciente_nacimiento" type="date" class="form-control{{ $errors->has('paciente_nacimiento') ? ' is-invalid' : '' }}" name="paciente_nacimiento" value="{{ old('paciente_nacimiento') }}" autofocus>
                                    @if ($errors->has('paciente_nacimiento'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('paciente_nacimiento') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(function() {
            $( "#paciente_nacimiento" ).datepicker({
                format: 'dd-mm-yyyy',
                
            });
        });
    
    </script> 
@endsection