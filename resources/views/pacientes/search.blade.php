@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session('message')) 
                <div class="alert alert-success">
                    {{session('message')}} 
                </div>
            @endif
            <hr>
            <div><b>Busqueda: {{$busqueda}}</b></div>
            <hr>
            @if (count($pacientes) > 0)
            @foreach ($pacientes as $paciente)
                <div class="card">
                    <div class="card-header">
                        <h4>
                            {{ $paciente->paciente_nombre }} {{ $paciente->paciente_apellido }}
                        </h4>
                    </div>
                    <div class="card-body">
                        <p>DNI: {{$paciente->paciente_dni}}</p>
                        <p>TEL: {{$paciente->paciente_telefono}}</p>
                        <p>Alta: {{$paciente->created_at}}</p>
                        <p>Modificación: {{$paciente->updated_at}}</p>
                        <p>
                            <a href="{{ route('ver_paciente', [ 'id' => $paciente->id ]) }}">VER</a>
                            <a href="{{ route('eliminar_paciente', [ 'id' => $paciente->id ]) }}">ELIMINAR</a>
                            <a href="{{ route('editar_paciente', [ 'id' => $paciente->id ]) }}">EDITAR</a>
                        </p>
                    </div>
                </div>
                <br>
            @endforeach
            <div class="clearfix"></div>
            {{$pacientes->links()}}
            @else
                <div class="alert alert-warning">No se encontraron resultados!!</div>    
            @endif
        </div>
    </div>
</div>  
@endsection   