@extends('layouts.app')
@section('title', 'Editar Paciente')
@section('title_top',"Editar: $paciente->paciente_nombre $paciente->paciente_apellido")

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8 col-md-7">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">Editar Paciente: <b><i>{{ strtoupper($paciente->paciente_nombre) }} {{ strtoupper($paciente->paciente_apellido) }}</i></b></div>
                    <div class="card-body">
                        <!--
                        <form method="POST" action="{{ route('actualizar_paciente', [ 'id' => $paciente->id ]) }}" class="text-left form-email" data-success="Paciente Actualizado" data-error="Faltan llenar campos.">
                            @csrf
                            <div class="col-sm-6"> <label>Nombre:</label> <input name="paciente_nombre" value="{{ $paciente->paciente_nombre }}" class="validate-required" type="text"> </div>
                            <div class="col-sm-6"> <label>Apellido:</label> <input name="paciente_apellido" value="{{ $paciente->paciente_apellido }}" class="validate-required" type="text"> </div>
                            <div class="col-sm-6"> <label>DNI:</label> <input name="paciente_dni" value="{{ $paciente->paciente_dni }}"  class="validate-required validate-number-dash" type="text"> </div>
                            <div class="col-sm-6"> <label>Teléfono:</label> <input name="paciente_telefono" value="{{ $paciente->paciente_telefono }}" type="text"> </div>
                            <div class="col-sm-12"> <label>Extra:</label> <textarea rows="6" name="paciente_extra">{{ $paciente->paciente_extra }}</textarea> </div>
                            <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                        </form>
                        -->
                        <form method="POST" action="{{ route('actualizar_paciente', [ 'id' => $paciente->id ]) }}" aria-label="Cargar Paciente">
                                @csrf
                                <div class="form-group row">
                                    <label for="paciente_nombre" class="col-md-4 col-form-label text-md-right">Nombre:</label>
        
                                    <div class="col-md-6">
                                        <input id="paciente_nombre" type="text" class="form-control{{ $errors->has('paciente_nombre') ? ' is-invalid' : '' }}" name="paciente_nombre" value="{{ $paciente->paciente_nombre }}" required autofocus>
        
                                        @if ($errors->has('paciente_nombre'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('paciente_nombre') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="paciente_apellido" class="col-md-4 col-form-label text-md-right">Apellido:</label>
                                    <div class="col-md-6">
                                        <input id="paciente_apellido" type="text" class="form-control{{ $errors->has('paciente_apellido') ? ' is-invalid' : '' }}" name="paciente_apellido" value="{{ $paciente->paciente_apellido }}" required autofocus>
                                        @if ($errors->has('paciente_apellido'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('paciente_apellido') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="paciente_dni" class="col-md-4 col-form-label text-md-right">DNI:</label>
                                    <div class="col-md-6">
                                        <input id="paciente_dni" type="text" class="form-control{{ $errors->has('paciente_dni') ? ' is-invalid' : '' }}" name="paciente_dni" value="{{ $paciente->paciente_dni }}" required autofocus>
                                        @if ($errors->has('paciente_dni'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('paciente_dni') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="paciente_telefono" class="col-md-4 col-form-label text-md-right">Teléfono:</label>
                                    <div class="col-md-6">
                                        <input id="paciente_telefono" type="text" class="form-control{{ $errors->has('paciente_telefono') ? ' is-invalid' : '' }}" name="paciente_telefono" value="{{ $paciente->paciente_telefono }}" required autofocus>
                                        @if ($errors->has('paciente_telefono'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('paciente_telefono') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="paciente_extra" class="col-md-4 col-form-label text-md-right">Extra:</label>
                                    <div class="col-md-6">
                                        <textarea id="paciente_extra" class="form-control{{ $errors->has('paciente_extra') ? ' is-invalid' : '' }}" name="paciente_extra" autofocus>{{ $paciente->paciente_extra }}</textarea>
                                        @if ($errors->has('paciente_extra'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('paciente_extra') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="paciente_nacimiento" class="col-md-4 col-form-label text-md-right">Fecha Nacimiento:</label>
                                    <div class="col-md-6">
                                        <input id="paciente_nacimiento" type="date" class="form-control{{ $errors->has('paciente_nacimiento') ? ' is-invalid' : '' }}" name="paciente_nacimiento" value="{{ $paciente->paciente_nacimiento }}" required autofocus>
                                        @if ($errors->has('paciente_nacimiento'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('paciente_nacimiento') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(function() {
            $( "#paciente_nacimiento" ).datepicker({
                format: 'dd-mm-yyyy',
                
            });
        });
    
    </script> 
@endsection