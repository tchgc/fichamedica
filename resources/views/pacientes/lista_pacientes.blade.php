@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session('message')) 
                <div class="alert alert-success">
                    {{session('message')}} 
                </div>
            @endif
            @if(session('error')) 
                <div class="alert alert-danger">
                    {{session('error')}} 
                </div>
            @endif
            <h3>Total de pacientes: {{count($pacientes)}}</h3>
            @if (count($pacientes) > 0)
            @foreach ($pacientes as $paciente)
                <div class="card">
                    <div class="card-header">
                        
                        <div class="row">
                            <div class="col-md-8">
                                <b>{{ strtoupper($paciente->paciente_nombre) }} {{ strtoupper($paciente->paciente_apellido) }}</b>
                            </div>
                            <div class="col-md-4">
                                @auth
                                    <a class="card-link" href="{{ route('ver_paciente', [ 'id' => $paciente->id ]) }}">VER</a>
                                    <a class="card-link" href="{{ route('editar_paciente', [ 'id' => $paciente->id ]) }}">EDITAR</a>
                                    <a class="card-link" href="#deletedModal{{$paciente->id}}" data-toggle="modal">ELIMINAR</a>
                                    @endauth
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p><b>DNI:</b> {{$paciente->paciente_dni}}</p>
                        <p><b>TEL:</b> {{$paciente->paciente_telefono}}</p>
                        <p><b>Fecha Nacimiento:</b> {{  date('d-m-Y', strtotime($paciente->paciente_nacimiento)) }}</p>
                        <p><b>Alta:</b> {{ \HowOld::LongTimeFilter($paciente->created_at) }}</p>
                            <!-- date('d-m-Y', strtotime($paciente->created_at)) -->
                        <p><b>Modificado:</b> {{ \HowOld::LongTimeFilter($paciente->updated_at) }}</p>
                    </div>
                </div>
                <br>
                <!-- Modal / Ventana / Overlay en HTML -->
                <div id="deletedModal{{$paciente->id}}" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title pull-left">¿Estás seguro?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>¿Seguro que quieres borrar a <b>{{ strtoupper($paciente->paciente_nombre) }} {{ strtoupper($paciente->paciente_apellido) }}</b>?</p>
                                <!--<p class="text-defauld"><small>Si lo borras, nunca podrás recuperarlo.</small></p>-->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <a href="{{ route('eliminar_paciente', [ 'id' => $paciente->id ]) }}" type="button" class="btn btn-danger">Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="clearfix"></div>
            {{$pacientes->links()}}
            @else
                <p>Todavía no hay pacientes cargados!! <a href="{{route('agregar_paciente')}}">Cargar Paciente</a></p>    
            @endif
        </div>
    </div>
</div>  
@endsection   
                    
                












