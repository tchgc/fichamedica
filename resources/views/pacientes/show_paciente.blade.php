@extends('layouts.app')
@section('title', 'Cargar Paciente')
@section('title_top',"$paciente->paciente_nombre $paciente->paciente_apellido")
@section('flash')
    @if(session('success')) 
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{session('success')}} 
        </div>
    @endif
@endsection
@section('content')
<!--
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-md-7">
                @if(session('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{session('success')}} 
                    </div>
                @endif
                <p>Nombre: {{ $paciente->paciente_nombre }}</p>
                <p>Apellido: {{ $paciente->paciente_apellido }}</p>
                <p>DNI: {{ $paciente->paciente_dni }}</p>
                <p>Telefono: {{ $paciente->paciente_telefono }}</p>
                <p>Extra: {{ $paciente->paciente_extra }}</p>
            </div>
        </div>
    </div>
-->
<!-- Modal / Ventana / Overlay en HTML -->
<div id="deletedModal{{$paciente->id}}" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title pull-left">¿Estás seguro?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>¿Seguro que quieres borrar a <b>{{ strtoupper($paciente->paciente_nombre) }} {{ strtoupper($paciente->paciente_apellido) }}</b>?</p>
                <!--<p class="text-defauld"><small>Si lo borras, nunca podrás recuperarlo.</small></p>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <a href="{{ route('eliminar_paciente', [ 'id' => $paciente->id ]) }}" type="button" class="btn btn-danger">Eliminar</a>
            </div>
        </div>
    </div>
</div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-9">
                                <b><i>{{ strtoupper($paciente->paciente_nombre) }} {{ strtoupper($paciente->paciente_apellido) }}</i></b>
                            </div>
                            <div class="col-md-3">
                                <a class="card-link" href="{{ route('editar_paciente', [ 'id' => $paciente->id ]) }}">EDITAR</a>
                                <a class="card-link" href="#deletedModal{{$paciente->id}}" data-toggle="modal">ELIMINAR</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p><b>Nombre:</b> {{ $paciente->paciente_nombre }}</p>
                        <p><b>Apellido:</b> {{ $paciente->paciente_apellido }}</p>
                        <p><b>DNI:</b> {{ $paciente->paciente_dni }}</p>
                        <p><b>Edad:</b> {{ \HowOld::edad($paciente->paciente_nacimiento) }}</p>
                        <p><b>Fecha Nacimiento:</b> {{  date('d-m-Y', strtotime($paciente->paciente_nacimiento)) }}</p>
                        <p><b>Telefono:</b> {{ $paciente->paciente_telefono }}</p>
                        <p><b>Extra:</b> {{ $paciente->paciente_extra }}</p>
                    </div>
                    <div class="card-footer text-muted">
                        Alta: {{  date('d-m-Y', strtotime($paciente->created_at)) }}
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                Visitas
                            </div>
                            <div class="col-md-2">
                                <a class="card-link" href="{{ route('show_visitas', [ 'paciente_id' => $paciente->id ]) }}">VER MÁS</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Fecha</th>
                                <th>Detalle</th>
                            </tr>
                            @foreach ($paciente->visitasLast as $visita)
                            <tr>
                                <td>{{  date('d-m-Y', strtotime($visita->created_at)) }}</td>
                                <td>{{ $visita->visita_detalle }}</td>
                            </tr>
                            @endforeach
                        </table>
                        <!--
                        <div class="pager">
                            {{ $paciente->visitasPaginate()->links() }}
                        </div>
                    -->
                        <br>
                        <div class="row">
                            <form method="POST" action="{{ route('cargar_visita', [ 'id_paciente' => $paciente->id ]) }}" class=" col-md-12 text-left">
                                <!--
                                <div class="col-md-12 col-sm-12"> <label>Nueva Visita:</label> <textarea rows="2" name="visita_detalle" required></textarea> </div>
                                <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                                -->
                                @csrf
                                <div class="form-group row">
                                    <label for="visita_detalle" class="col-md-12 col-form-label">Nueva Visita:</label>
        
                                    <div class="col-md-12">
                                        <textarea rows="2" id="visita_detalle" class="form-control{{ $errors->has('visita_detalle') ? ' is-invalid' : '' }}" name="visita_detalle" required autofocus>{{ old('visita_detalle') }}</textarea>
                                        @if ($errors->has('visita_detalle'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('visita_detalle') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                            </form> 
                        </div>       
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                Remedios
                            </div>
                            <div class="col-md-2">
                                <a class="card-link" href="{{ route('show_remedios', [ 'paciente_id' => $paciente->id ]) }}">VER MÁS</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Fecha</th>
                                <th>Nombre</th>
                                <th>Droga</th>
                            </tr>
                            @foreach ($paciente->remediosLast as $remedio)
                            <tr><!-- 'd-m-Y h:i'-->
                                <td>{{  date('d-m-Y', strtotime($remedio->created_at)) }}</td>
                                <td>{{ $remedio->remedio_nombre }}</td>
                                <td>{{ $remedio->remedio_droga }}</td>
                            </tr>
                            @endforeach
                        </table>
                        <br>
                        <div class="row">
                            <form method="POST" action="{{ route('cargar_remedio', [ 'id_paciente' => $paciente->id ]) }}" class=" col-md-12 text-left">
                                <!--
                                <div class="col-sm-6"> <label>Nombre:</label> <input name="remedio_nombre" type="text" required> </div>
                                <div class="col-sm-6"> <label>Droga:</label> <input name="remedio_droga" type="text" required> </div>
                                <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                                -->
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="remedio_nombre" class="col-form-label">Nombre:</label>
                                        <input id="remedio_nombre" type="text" class="form-control{{ $errors->has('remedio_nombre') ? ' is-invalid' : '' }}" name="remedio_nombre" value="{{ old('remedio_nombre') }}" required autofocus>
                                        @if ($errors->has('remedio_nombre'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('remedio_nombre') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label for="remedio_droga" class="col-form-label">Droga:</label>
                                        <input id="remedio_droga" type="text" class="form-control{{ $errors->has('remedio_droga') ? ' is-invalid' : '' }}" name="remedio_droga" value="{{ old('remedio_droga') }}" required autofocus>
                                        @if ($errors->has('remedio_droga'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('remedio_droga') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                            </form> 
                        </div>       
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                Estudios
                            </div>
                            <div class="col-md-2">
                                <a class="card-link" href="{{ route('show_estudios', [ 'paciente_id' => $paciente->id ]) }}">VER MÁS</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th>Fecha</th>
                                <th>Detalle</th>
                            </tr>
                            @foreach ($paciente->estudiosLast as $estudio)
                            <tr>
                                <td>{{  date('d-m-Y', strtotime($estudio->created_at)) }}</td>
                                <td>{{ $estudio->estudio_detalle }}</td>
                            </tr>
                            @endforeach
                        </table>
                        <br>
                        <div class="row">
                            <form method="POST" action="{{ route('cargar_estudio', [ 'id_paciente' => $paciente->id ]) }}" class=" col-md-12 text-left">
                                <!--
                                <div class="col-md-12 col-sm-12"> <label>Nuevo Estudio:</label> <textarea rows="2" name="estudio_detalle" required></textarea> </div>
                                <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                                -->
                                @csrf
                                <div class="form-group row">
                                    <label for="estudio_detalle" class="col-md-12 col-form-label">Nuevo Estudio:</label>
        
                                    <div class="col-md-12">
                                        <textarea rows="2" id="estudio_detalle" class="form-control{{ $errors->has('estudio_detalle') ? ' is-invalid' : '' }}" name="estudio_detalle" required autofocus>{{ old('estudio_detalle') }}</textarea>
        
                                        @if ($errors->has('estudio_detalle'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('estudio_detalle') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-5 col-md-4"> <button type="submit" class="btn btn--primary type--uppercase">Guardar </button> </div>
                            </form> 
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection