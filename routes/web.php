<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});


Route::get('contacto/{nombre}', function($nombre = "Por defecto"){
    
    return view('contacto',array(
        "nombre" =>$nombre
    ));
    
    return view('contacto')->whit('nombre', $nombre);
})->where([
    'nombre' => '[A-Za-z]+'
]);
*/
Route::get('/',[
    'as' => 'lista_paciente',
    'uses' => 'PacientesController@index']);

Route::get('/search-paciente/{search?}',[
    'as' => 'search_paciente',
    'uses' => 'PacientesController@search']);
    
Route::get('/pacientes/add',[
    'as' => 'agregar_paciente',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@addPaciente']);

Route::post('/pacientes/store',[
    'as' => 'guardar_paciente',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@storePaciente']);

Route::get('/pacientes/show/{id}',[
    'as' => 'ver_paciente',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@showPaciente'])->where(['id' => '[0-9]+']);

Route::post('/pacientes/update/{id}',[
    'as' => 'actualizar_paciente',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@updatePaciente'])->where(['id' => '[0-9]+']);

Route::get('/pacientes/edit/{id}',[
    'as' => 'editar_paciente',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@editPaciente'])->where(['id' => '[0-9]+']);

Route::get('/pacientes/delete/{id}',[
    'as' => 'eliminar_paciente',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@deletePaciente'])->where(['id' => '[0-9]+']);
    
Route::post('/pacientes/{id_paciente}/new-visita',[
    'as' => 'cargar_visita',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@storeVisita'])->where(['id_paciente' => '[0-9]+']);

Route::post('/pacientes/{id_paciente}/new-remedio',[
    'as' => 'cargar_remedio',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@storeRemedio'])->where(['id_paciente' => '[0-9]+']);

Route::post('/pacientes/{id_paciente}/new-estudio',[
    'as' => 'cargar_estudio',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@storeEstudio'])->where(['id_paciente' => '[0-9]+']);

Route::get('/pacientes/{id_paciente}/visitas',[
    'as' => 'show_visitas',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@showVisitas'])->where(['id_paciente' => '[0-9]+']);

Route::get('/pacientes/{id_paciente}/estudios',[
    'as' => 'show_estudios',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@showEstudios'])->where(['id_paciente' => '[0-9]+']);

Route::get('/pacientes/{id_paciente}/remedios',[
    'as' => 'show_remedios',
    'middleware'=> 'auth',
    'uses' => 'PacientesController@showRemedios'])->where(['id_paciente' => '[0-9]+']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Limpiar cahce
Route::get('/clear-cache', function(){
    $code = Artisan::call('cache:clear');
});
