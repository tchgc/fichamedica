<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pacientes';

    /**
     * Get the estudios for the paciente.
     */
    public function estudios()
    {
        return $this->hasMany('App\Estudio','paciente_id','id')->orderBy('created_at', 'desc');
    }

    public function estudiosLast()
    {
        return $this->hasMany('App\Estudio','paciente_id','id')->orderBy('created_at', 'desc')->limit(10);
    }

    /**
     * Get the remedios for the paciente.
     */
    public function remedios()
    {
        return $this->hasMany('App\Remedio','paciente_id','id')->orderBy('created_at', 'desc');
    }

    public function remediosLast()
    {
        return $this->hasMany('App\Remedio','paciente_id','id')->orderBy('created_at', 'desc')->limit(10);
    }

    /**
     * Get the visitas for the paciente.
     */
    public function visitas()
    {
        return $this->hasMany('App\Visita','paciente_id','id')->orderBy('created_at', 'desc');
    }

    public function visitasLast()
    {
        return $this->hasMany('App\Visita','paciente_id','id')->orderBy('created_at', 'desc')->limit(10);
    }

    public function visitasPaginate()
    {
        return $this->hasMany('App\Visita','paciente_id','id')->orderBy('created_at', 'desc')->paginate(5);
    }
}
