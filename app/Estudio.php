<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudio extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estudios';

     /**
     * Get the paciente that owns the estudio.
     */
    public function paciente()
    {
        return $this->belongsTo('App\Paciente', 'paciente_id', 'paciente_id');
    }
}
