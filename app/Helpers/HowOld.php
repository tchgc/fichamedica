<?php

namespace App\Helpers;
  
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class  HowOld {

    public static function Age($date) {
        if ($date == null) {
            return "Sin fecha";
        }
        
        $split_date = explode('-',$date);
        
        return  Carbon::createFromDate($split_date[0],$split_date[1],$split_date[2])->age;
    }

    public static function edad($date) {
        $fecha_de_nacimiento = $date;
        $fecha_actual = date ("Y-m-d");

        // separamos en partes las fechas
        $array_nacimiento = explode ( "-", $fecha_de_nacimiento );
        $array_actual = explode ( "-", $fecha_actual );

        $anos =  $array_actual[0] - $array_nacimiento[0]; // calculamos años
        $meses = $array_actual[1] - $array_nacimiento[1]; // calculamos meses
        $dias =  $array_actual[2] - $array_nacimiento[2]; // calculamos días

        //ajuste de posible negativo en $días
        if ($dias < 0)
        {
            --$meses;

            //ahora hay que sumar a $dias los dias que tiene el mes anterior de la fecha actual
            switch ($array_actual[1]) {
                case 1:     $dias_mes_anterior=31; break;
                case 2:     $dias_mes_anterior=31; break;
                case 3: 
                        if (bisiesto($array_actual[0]))
                        {
                            $dias_mes_anterior=29; break;
                        } else {
                            $dias_mes_anterior=28; break;
                        }
                case 4:     $dias_mes_anterior=31; break;
                case 5:     $dias_mes_anterior=30; break;
                case 6:     $dias_mes_anterior=31; break;
                case 7:     $dias_mes_anterior=30; break;
                case 8:     $dias_mes_anterior=31; break;
                case 9:     $dias_mes_anterior=31; break;
                case 10:     $dias_mes_anterior=30; break;
                case 11:     $dias_mes_anterior=31; break;
                case 12:     $dias_mes_anterior=30; break;
            }

            $dias=$dias + $dias_mes_anterior;
        }

        //ajuste de posible negativo en $meses
        if ($meses < 0)
        {
            --$anos;
            $meses=$meses + 12;
        }

        $texto_dias = 'días';
        $texto_meses = 'meses';
        $texto_anos = 'años'; 

        if($dias == 1){
            $texto_dias = 'día';    
        }

        if($meses == 1){
            $texto_meses = 'mes';    
        } 

        if($anos == 1){
            $texto_anos = 'año';    
        }

        if ($anos <= 0) {
            return $meses.' '.$texto_meses.' y '.$dias.' '.$texto_dias;
        } else {
            return $anos.' '.$texto_anos .' con '.$meses.' '.$texto_meses.' y '.$dias.' '.$texto_dias;
        }
    }

    public static function LongTimeFilter($date) {
        if ($date == null) {
            return "Sin fecha";
        }
 
        $start_date = $date;
        $since_start = $start_date->diff(new \DateTime(date("Y-m-d") . " " . date("H:i:s")));
 
        if ($since_start->y == 0) {
            if ($since_start->m == 0) {
                if ($since_start->d == 0) {
                    if ($since_start->h == 0) {
                        if ($since_start->i == 0) {
                            if ($since_start->s == 0) {
                                $result = $since_start->s . ' segundos';
                            } else {
                                if ($since_start->s == 1) {
                                    $result = $since_start->s . ' segundo';
                                } else {
                                    $result = $since_start->s . ' segundos';
                                }
                            }
                        } else {
                            if ($since_start->i == 1) {
                                $result = $since_start->i . ' minuto';
                            } else {
                                $result = $since_start->i . ' minutos';
                            }
                        }
                    } else {
                        if ($since_start->h == 1) {
                            $result = $since_start->h . ' hora';
                        } else {
                            $result = $since_start->h . ' horas';
                        }
                    }
                } else {
                    if ($since_start->d == 1) {
                        $result = $since_start->d . ' día';
                    } else {
                        $result = $since_start->d . ' días';
                    }
                }
            } else {
                if ($since_start->m == 1) {
                    $result = $since_start->m . ' mes';
                } else {
                    $result = $since_start->m . ' meses';
                }
            }
        } else {
            if ($since_start->y == 1) {
                $result = $since_start->y . ' año';
            } else {
                $result = $since_start->y . ' años';
            }
        }
 
        return "Hace " . $result;
    }

    function bisiesto($anio_actual){
        $bisiesto=false;
        //probamos si el mes de febrero del año actual tiene 29 días
        if (checkdate(2,29,$anio_actual))
        {
            $bisiesto=true;
        }
        return $bisiesto;
    } 
}