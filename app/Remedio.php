<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remedio extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'remedios';

    /**
     * Get the paciente that owns the remedio.
     */
    public function paciente()
    {
        return $this->belongsTo('App\Paciente', 'paciente_id', 'paciente_id');
    }
}
