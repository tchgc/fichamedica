<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'visitas';

     /**
     * Get the paciente that owns the visita.
     */
    public function paciente()
    {
        return $this->belongsTo('App\Paciente', 'id', 'paciente_id');
    }

    
}
