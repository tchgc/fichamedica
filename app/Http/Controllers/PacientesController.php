<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Paciente;
use App\Visita;
use App\Remedio;
use App\Estudio;
use Carbon\Carbon;
 

class PacientesController extends Controller
{
    public function index()
    {
        //$pacientes = DB::table('pacientes')->get();
        //$pacientes = Paciente::all();
        $pacientes = Paciente::orderBy('id', 'desc')->paginate(5);
        
        return view('pacientes.lista_pacientes', [
            'pacientes' => $pacientes
            ]);
    }

    public function deletePaciente($id)
    {
        //$pacientes = DB::table('pacientes')->where('paciente_id',$id)->delete();
        //$pacientes = Paciente::find($id);
        //$pacientes->delete();
        Paciente::destroy($id);
        
        return redirect()->action('PacientesController@index')->with('message', 'Paciente eliminado!!');
    }

    public function updatePaciente(Request $request, $id)
    {
        /*
        $pacientes = DB::table('pacientes')->where('paciente_id',$id)->update(array(
            'paciente_nombre' => $request->input('paciente_nombre'),
            'paciente_apellido' => $request->input('paciente_apellido'),
            'paciente_dni' => $request->input('paciente_dni'),
            'paciente_telefono' => $request->input('paciente_telefono'),
            'paciente_extra' => $request->input('paciente_extra')
        ));
        */
        $validatedData = $this->validate($request, [
            'paciente_nombre' => 'required|max:255',
            'paciente_apellido' => 'required|max:255',
            'paciente_dni' => 'digits_between:5,15|max:255',
            'paciente_nacimiento' => 'date',
            'paciente_telefono' => 'digits_between:5,15|max:255',
            'paciente_extra' => 'max:255',
        ]);

        $paciente = Paciente::find($id);

        $paciente->paciente_nombre = $request->paciente_nombre;
        $paciente->paciente_apellido = $request->paciente_apellido;
        $paciente->paciente_dni = $request->paciente_dni;
        $paciente->paciente_nacimiento = $request->paciente_nacimiento;
        $paciente->paciente_telefono = $request->paciente_telefono;
        $paciente->paciente_extra = $request->paciente_extra;

        $paciente->update();

        //return response()->json(array('status' => 'success', 'url'=> url('/pacientes/show/'.$id)), 200);
        return redirect()->route('ver_paciente',['id' => $paciente->id])->with('status', 'Paciente '.$paciente->paciente_nombre.' '.$paciente->paciente_apellido.' actualizado correctamente!!');
    }

    public function editPaciente( $id)
    {
        //$paciente = DB::table('pacientes')->where('paciente_id',$id)->first();
        $paciente = Paciente::find($id);
        if (!empty($paciente)) {
            return view('pacientes.edit_paciente',['paciente' => $paciente]);
        } else {
            //return redirect('/')->with('error','El paciente no existe!!');
            return redirect()->route('lista_paciente')->with('error','El paciente no existe!!');
        }
    }

    public function storePaciente(Request $request)
    {
       /* 
        $paciente_id = DB::table('pacientes')->insertGetId(array(
            'paciente_nombre' => $request->input('paciente_nombre'),
            'paciente_apellido' => $request->input('paciente_apellido'),
            'paciente_dni' => $request->input('paciente_dni'),
            'paciente_telefono' => $request->input('paciente_telefono'),
            'paciente_extra' => $request->input('paciente_extra')
            
        ));
        */

       // $validatedData = $request->validate([
           
        $validatedData = $this->validate($request, [
            'paciente_nombre' => 'required|max:255',
            'paciente_apellido' => 'required|max:255',
            'paciente_dni' => 'digits_between:5,15|max:255',
            'paciente_nacimiento' => 'date',
            'paciente_telefono' => 'digits_between:5,15|max:255',
            'paciente_extra' => 'max:255',
            'paciente_sexo' => 'required|in:"Hombre","Mujer"'
        ]);
        
        $paciente = new Paciente;

        $paciente->paciente_nombre = $request->paciente_nombre;
        $paciente->paciente_apellido = $request->paciente_apellido;
        $paciente->paciente_dni = $request->paciente_dni;
        $paciente->paciente_nacimiento = $request->paciente_nacimiento;
        $paciente->paciente_telefono = $request->paciente_telefono;
        $paciente->paciente_extra = $request->paciente_extra;
        $paciente->paciente_sexo = $request->paciente_sexo;

        $paciente->save();

        //return redirect()->action('PacientesController@index')->with('successs', 'Paciente cargado correctamente!!');
        //return redirect()->action('PacientesController@showPaciente',['id' => $paciente_id]);
        //return url('/pacientes/show/'.$paciente_id);
        //return response()->json(array('status' => 'success', 'url'=> url('/pacientes/show/'.$paciente->id)), 200);
        return redirect()->route('ver_paciente',[
            'id' => $paciente->id
            ])->with('status', 'Paciente '.$paciente->paciente_nombre.' '.$paciente->paciente_apellido.' cargado correctamente!!');
        
        /*
        $this->validate($request, [
            'paciente_nombre' => 'required|max:255',
            'paciente_apellido' => 'required|max:255',
            'paciente_dni' => 'required|max:255|',
            'paciente_telefono' => 'max:255',
            'paciente_extra' => 'max:255',
        ]);
        $user = Pacientes::create($request->all());
        */
    }

    public function addPaciente()
    {
        return view('pacientes.add_pacientes');
    }

    public function showPaciente($id)
    {
        //$paciente = DB::table('pacientes')->where('paciente_id',$id)->first();
        $paciente = Paciente::find($id);
        if (!empty($paciente)) {
            return view('pacientes.show_paciente',['paciente' => $paciente]);
        } else {
            //return redirect('/')->with('error','El paciente no existe!!');
            return redirect()->route('lista_paciente')->with('error','El paciente no existe!!');
        }
        
    }

    public function storeVisita(Request $request, $id_paciente)
    {
        $validatedData = $this->validate($request, [
            'visita_detalle' => 'required|min:10|max:255'
        ]);

        $visita = new Visita;

        $visita->visita_detalle = $request->visita_detalle;
        //$visita->visita_fecha = $request->visita_fecha;

        $paciente = Paciente::find($id_paciente);
        $paciente->visitas()->save($visita);

        return redirect()->action('PacientesController@showPaciente',['id' => $id_paciente])->with('status', 'Visita cargada correctamente!!');
    }

    public function storeRemedio(Request $request, $id_paciente)
    {
        $validatedData = $this->validate($request, [
            'remedio_nombre' => 'required|min:3|max:255',
            'remedio_droga' => 'required|min:3|max:255'
        ]);

        $remedio = new Remedio;

        $remedio->remedio_nombre = $request->remedio_nombre;
        $remedio->remedio_droga = $request->remedio_droga;
        //$visita->visita_fecha = $request->visita_fecha;

        $paciente = Paciente::find($id_paciente);
        $paciente->remedios()->save($remedio);

        return redirect()->action('PacientesController@showPaciente',['id' => $id_paciente])->with('status', 'Remedio cargado correctamente!!');
    }

    public function storeEstudio(Request $request, $id_paciente)
    {

        $validatedData = $this->validate($request, [
            'estudio_detalle' => 'required|min:5|max:255'
        ]);

        $estudio = new Estudio;

        $estudio->estudio_detalle = $request->estudio_detalle;
        //$visita->visita_fecha = $request->visita_fecha;

        $paciente = Paciente::findOrFail($id_paciente);
        $paciente->estudios()->save($estudio);

        return redirect()->action('PacientesController@showPaciente',['id' => $id_paciente])->with('status', 'Estudio cargado correctamente!!');
    }


    public function search($search = null)
    {
        if(empty($search) && \Request::get('search')){
            $search = \Request::get('search');

            return redirect()->route('search_paciente', [ 'search' => $search ]);
        }

        $result = Paciente::where('paciente_nombre', 'LIKE', '%'.$search.'%')
        ->orWhere('paciente_apellido', 'LIKE', '%'.$search.'%')
        ->orWhere('paciente_telefono', 'LIKE', '%'.$search.'%')
        ->orWhere('paciente_nacimiento', 'LIKE', '%'.$search.'%')
        ->orWhere('paciente_dni', 'LIKE', '%'.$search.'%')
        ->orWhere('paciente_extra', 'LIKE', '%'.$search.'%')
        ->paginate(7);

        return view('pacientes.search',[
            'pacientes' => $result,
            'busqueda' => $search 
            ]);
    }

    public function showVisitas($paciente_id)
    {
        //$visitas = Paciente::with('visitas')->find($paciente_id)->visitasAll;

        $paciente = Paciente::find($paciente_id);
        if (!empty($paciente)) {
            $visitas = $paciente->visitas()->paginate(15);
            //$visitas = Visita::where('paciente_id',$paciente_id)->orderBy('created_at', 'desc')->paginate(20);
            return view('visitas.show_visitas',[
                'visitas' => $visitas,
                'paciente_id' => $paciente_id,
                'paciente' => $paciente
                ]);
        } else {
            //return redirect('/')->with('error','El paciente no existe!!');
            return redirect()->route('lista_paciente')->with('error','El paciente no existe!!');
        }
        
    }

    public function showRemedios($paciente_id)
    {
        $paciente = Paciente::find($paciente_id);
        if (!empty($paciente)) {
            $remedios = $paciente->remedios()->paginate(15);
            return view('remedios.show_remedios',[
                'remedios' => $remedios,
                'paciente_id' => $paciente_id,
                'paciente' => $paciente
                ]);
        } else {
            return redirect()->route('lista_paciente')->with('error','El paciente no existe!!');
        }
        
    }

    public function showEstudios($paciente_id)
    {
        $paciente = Paciente::find($paciente_id);
        if (!empty($paciente)) {
            $estudios = $paciente->estudios()->paginate(15);
            return view('estudios.show_estudios',[
                'estudios' => $estudios,
                'paciente_id' => $paciente_id,
                'paciente' => $paciente
                ]);
        } else {
            return redirect()->route('lista_paciente')->with('error','El paciente no existe!!');
        }
    }


}
